package com.stylingandroid.cloudsave;

import android.app.Activity;
import android.content.Intent;
import android.content.IntentSender;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import com.google.android.gms.appstate.AppStateClient;
import com.google.android.gms.appstate.OnStateLoadedListener;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.common.Scopes;

import java.nio.charset.Charset;

public class MainActivity extends Activity implements GooglePlayServicesClient.ConnectionCallbacks, GooglePlayServicesClient.OnConnectionFailedListener, OnStateLoadedListener
{
    private static final Charset charset = Charset.forName("UTF-8");

    private static final String TAG = "Cloud Save";
    private static final int RC_RESOLVE = 9001;

    private AppStateClient appStateClient = null;

    private boolean connecting = false;

    private EditText persistent = null;
    private TextView messages = null;
    private Button saveButton = null;
    private Button loadButton = null;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        persistent = (EditText) findViewById(android.R.id.text1);
        messages = (TextView) findViewById(android.R.id.text2);
        saveButton = (Button) findViewById(R.id.saveButton);
        loadButton = (Button) findViewById(R.id.loadButton);

        String[] scopes = new String[]{Scopes.APP_STATE};

        appStateClient = new AppStateClient.Builder(this, this, this)
                .setScopes(scopes)
                .create();
        messages.setText(getString(R.string.connecting));
    }

    @Override
    protected void onStart()
    {
        super.onStart();
        if (!connecting)
        {
            connect();
        }
    }

    @Override
    protected void onStop()
    {
        disconnect();
        super.onStop();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    private void connect()
    {
        connecting = true;
        appStateClient.connect();
    }

    private void disconnect()
    {
        appStateClient.disconnect();
    }

    @Override
    public void onConnected(Bundle bundle)
    {
        messages.setText("Connected");
        loadState(null);
    }

    @Override
    public void onDisconnected()
    {
        saveButton.setEnabled(false);
        loadButton.setEnabled(false);
        messages.setText("Disconnected");
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult)
    {
        messages.setText("Connection failed: " + connectionResult.getErrorCode());
        if (connectionResult.hasResolution())
        {
            messages.setText("Attempting to resolve connection failure: " + connectionResult.getErrorCode());
            try
            {
                connecting = true;
                connectionResult.startResolutionForResult(this, RC_RESOLVE);
            } catch (IntentSender.SendIntentException e)
            {
                appStateClient.connect();
            }
        } else
        {
            messages.setText("Unresolvable connection failure: " + connectionResult.getErrorCode());
        }
    }

    public void onActivityResult(int requestCode, int responseCode, Intent intent)
    {
        if (requestCode == RC_RESOLVE)
        {
            connecting = false;
            Log.d(TAG, "onActivityResult, req " + requestCode + " response " + responseCode);
            if (responseCode == Activity.RESULT_OK)
            {
                appStateClient.connect();
            } else
            {
                Log.e(TAG, "Unable to connect");
                connecting = false;
            }
        }
    }

    @Override
    public void onStateLoaded(int i, int i2, byte[] bytes)
    {
        saveButton.setEnabled(true);
        loadButton.setEnabled(true);
        if (bytes != null)
        {
            persistent.setText(new String(bytes, charset));
        }
    }

    @Override
    public void onStateConflict(int i, String s, byte[] bytes, byte[] bytes2)
    {

    }

    public void saveState(View view)
    {
        saveButton.setEnabled(false);
        loadButton.setEnabled(false);
        Log.v(TAG, "Max keys: " + appStateClient.getMaxNumKeys());
        appStateClient.updateStateImmediate(this, 0, persistent.getText().toString().getBytes(charset));
    }

    public void loadState(View view)
    {
        saveButton.setEnabled(false);
        loadButton.setEnabled(false);
        appStateClient.loadState(this, 0);
    }
}
